﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TestV8Web.Configuracion;
using TestV8Web.Models;
using TestV8Web.Service;

namespace TestV8Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IService _service;
        public IOptionsSnapshot<AppSettings> ApiConfig { get; }
        public HomeController(ILogger<HomeController> logger, IService service, IOptionsSnapshot<AppSettings> apiConfig)
        {
            ApiConfig = apiConfig;
            _logger = logger;
            _service = service;
        }

        public async Task<IActionResult> Index()
        {
            await LoadInformation();
            return View();
        }

        private async Task LoadInformation(Boolean isFilter = true)
        {
            ServiceResponse<String> serviceResponse = await _service.GetAsync(ApiConfig.Value.Domain_ApiSalary + AppParameters.ServiceSalary.GetOffice);
            ServiceResponse<BaseEntityDto> modelList = JsonConvert.DeserializeObject<ServiceResponse<BaseEntityDto>>(serviceResponse.Data);
            if (isFilter)
                modelList.DataList.Insert(0, new BaseEntityDto { Id = 0, Name = "Todos" });
            ViewBag.ListOffice = modelList.DataList;

            serviceResponse = await _service.GetAsync(ApiConfig.Value.Domain_ApiSalary + AppParameters.ServiceSalary.GetPosition);
            modelList = JsonConvert.DeserializeObject<ServiceResponse<BaseEntityDto>>(serviceResponse.Data);
            ViewBag.ListPosition = modelList.DataList;

            serviceResponse = await _service.GetAsync(ApiConfig.Value.Domain_ApiSalary + AppParameters.ServiceSalary.GetDivision);
            modelList = JsonConvert.DeserializeObject<ServiceResponse<BaseEntityDto>>(serviceResponse.Data);
            ViewBag.ListDivision = modelList.DataList;
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        [HttpGet]
        public async Task<JsonResult> GetByEmployeeCode([FromQuery]SalaryFilterDto data)
        {
            ServiceResponse<String> listSalary = await _service.GetAsync(
                String.Format(ApiConfig.Value.Domain_ApiSalary + AppParameters.ServiceSalary.GetByEmployeee, data.EmployeeCode)
                );
            ServiceResponse<SalaryEmployeeDto> response = JsonConvert.DeserializeObject<ServiceResponse<SalaryEmployeeDto>>(listSalary.Data);
            return Json(response);
        }
        [HttpGet]
        public async Task<JsonResult> GetByFilters([FromQuery]SalaryFilterDto data)
        {

            ServiceResponse<String> listSalary = await _service.GetAsync(String.Format(ApiConfig.Value.Domain_ApiSalary + AppParameters.ServiceSalary.GetFilters,
                                                                    data.FilterType, data.Position, data.Office, data.Grade, data.Division));
            ServiceResponse<SalaryEmployeeDto> response = JsonConvert.DeserializeObject<ServiceResponse<SalaryEmployeeDto>>(listSalary.Data);
            return Json(response);
        }

        public async Task<IActionResult> Salary()
        {
            await LoadInformation(false);
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> SaveMasive(String elemento)
        {
            IList<SalaryDto> modelData = JsonConvert.DeserializeObject<IList<SalaryDto>>(elemento);
            SalaryListDto salaryListDto = new SalaryListDto
            {
                SalaryListsDto = modelData
            };
            ServiceResponse<String> insertResponse = await _service.PostAsync(ApiConfig.Value.Domain_ApiSalary + AppParameters.ServiceSalary.SaveMasive, salaryListDto);
            if (insertResponse.Success)
            {
                ServiceResponse<SalaryDto> response = JsonConvert.DeserializeObject<ServiceResponse<SalaryDto>>(insertResponse.Data);
                return Json(response);
            }
            else
            {
                return Json(insertResponse);
            }
        }
        [HttpGet]
        public async Task<JsonResult> GetAll()
        {
            ServiceResponse<String> listSalary = await _service.GetAsync(ApiConfig.Value.Domain_ApiSalary + AppParameters.ServiceSalary.GetSalaryAll);
            ServiceResponse<SalaryEmployeeDto> response = JsonConvert.DeserializeObject<ServiceResponse<SalaryEmployeeDto>>(listSalary.Data);
            return Json(response);
        }
    }
}
