﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using TestV8Web.Models;

namespace TestV8Web.Service
{
    public class Service : IService
    {
        public Service()
        {
        }
        static HttpClient _apiClient = new HttpClient();
        public async Task<ServiceResponse<String>> GetAsync(String path)
        {
            String resultData = String.Empty;
            HttpResponseMessage result = await _apiClient.GetAsync(path);
            if (result.IsSuccessStatusCode)
                resultData = await result.Content.ReadAsStringAsync();
            ServiceResponse<String> response = new ServiceResponse<String>
            {
                Data = resultData,
                Success = result.IsSuccessStatusCode,
                Message = result.StatusCode.ToString(),
                CodeResult = (Int16)result.StatusCode
            };
            return response;

        }
        public async Task<ServiceResponse<String>> PostAsync(String path, Object objectInput)
        {
            String resultData = String.Empty;
            var contentString = new StringContent(JsonConvert.SerializeObject(objectInput), System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage result = await _apiClient.PostAsync(path, contentString);
            if (result.IsSuccessStatusCode)
                resultData = await result.Content.ReadAsStringAsync();
            if ((Int16)result.StatusCode == 400)
            {
                resultData = result.Content.ReadAsStringAsync().Result;
            }
            ServiceResponse<String> response = new ServiceResponse<String>
            {
                Data = resultData,
                Success = result.IsSuccessStatusCode,
                Message = result.StatusCode.ToString(),
                CodeResult = (Int16)result.StatusCode
            };
            return response;
        }
    }
}
