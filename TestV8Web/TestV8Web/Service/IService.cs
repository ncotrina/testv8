﻿using System;
using System.Threading.Tasks;
using TestV8Web.Models;

namespace TestV8Web.Service
{
    public interface IService
    {
        Task<ServiceResponse<String>> GetAsync(String path);
        Task<ServiceResponse<String>> PostAsync(String path, Object objectInput);
    }
}
