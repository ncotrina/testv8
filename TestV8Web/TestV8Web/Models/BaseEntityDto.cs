﻿using System;

namespace TestV8Web.Models
{
    public class BaseEntityDto
    {
        public Int16 Id { get; set; }
        public String Name { get; set; }
    }
}
