﻿using System.Collections.Generic;

namespace TestV8Web.Models
{
    public class SalaryListDto
    {
        public IList<SalaryDto> SalaryListsDto { get; set; }
    }
}
