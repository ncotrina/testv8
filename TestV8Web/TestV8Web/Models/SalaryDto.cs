﻿using System;

namespace TestV8Web.Models
{
    public class SalaryDto
    {
        public Int32 index { get; set; }
        public Int32 Year { get; set; }
        public Int32 Month { get; set; }
        public String EmployeeCode { get; set; }
        public String EmployeeName { get; set; }
        public String EmployeeSurname { get; set; }
        public Int32 Grade { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime Birthday { get; set; }
        public String IdentificationNumber { get; set; }
        public Decimal BaseSalary { get; set; }
        public Decimal ProductionBonus { get; set; }
        public Decimal CompensationBonus { get; set; }
        public Decimal Commission { get; set; }
        public Decimal Contributions { get; set; }
        public BaseEntityDto Position { get; set; }
        public BaseEntityDto Office { get; set; }
        public BaseEntityDto Division { get; set; }
    }
}
