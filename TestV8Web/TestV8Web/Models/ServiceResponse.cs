﻿using System;
using System.Collections.Generic;

namespace TestV8Web.Models
{
    public class ServiceResponse<T>
    {
        public Boolean Success { get; set; }
        public Int16 CodeResult { get; set; }
        public String Message { get; set; }
        public T Data { get; set; }
        public IList<T> DataList { get; set; }
    }
}
