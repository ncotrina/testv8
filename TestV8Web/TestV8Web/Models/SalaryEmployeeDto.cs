﻿using System;

namespace TestV8Web.Models
{
    public class SalaryEmployeeDto
    {
        public String EmployeeCode { get; set; }
        public String EmployeeFullName { get; set; }
        public BaseEntityDto Division { get; set; }
        public BaseEntityDto Position { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime Birthday { get; set; }
        public String IdentificationNumber { get; set; }
        public Decimal TotalSalary { get; set; }
    }
}
