﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestV8Web.Configuracion
{
    public static class AppParameters
    {
        public struct ServiceSalary
        {
            public const String SaveMasive = "/api/Salary";
            public const String GetFilters = "/api/Salary/filters?FilterType={0}&Position={1}&Office={2}&Grade={3}&Division={4}";
            public const String GetByEmployeee = "/api/Salary/employee?EmployeeCode={0}";
            public const String GetOffice = "/api/Office";
            public const String GetPosition = "/api/Position";
            public const String GetDivision = "/api/Division";
            public const String GetSalaryAll = "/api/Salary";

        }
    }
}
