﻿'use strict';
export function baseJs() {
    let base = {
        Fn_FormatMonth(d) {
            return (d < 10 ? '0' : '') + d;
        },
        Fn_LoadHandlebarTemplate: function (templateId, jsonObject) {
            Handlebars.registerHelper("if", function (conditional, options) {
                if (conditional) {
                    return options.fn(this);
                } else {
                    return options.inverse(this);
                }
            });
            Handlebars.registerHelper('ifCond', function (v1, v2, options) {
                if (v1 > v2) {
                    return options.fn(this);
                }
                return options.inverse(this);
            });
            Handlebars.registerHelper('ifEquals', function (v1, v2, options) {
                if (v1 === v2) {
                    return options.fn(this);
                }
                return options.inverse(this);
            });
            Handlebars.registerHelper('DateFormat', function (date) {
                let dt = new Date(date);
                return base.Fn_FormatMonth(dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
            });
            Handlebars.registerHelper('incremented', function (index) {
                index++
                return index;
            });
            let stemplate = $("#" + templateId).html();
            let tmpl = Handlebars.compile(stemplate);
            return tmpl(jsonObject);
        },
        Fn_CallMethod: function (url, data, success, error, type) {
            $.ajax({
                type: type,
                url: url,
                data: data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: success,
                error: error
            });
        },
        Fn_CallMethodPost: function (url, objData, success, error) {
            $.ajax({
                type: "POST",
                url: url,
                data: { elemento: objData },
                dataType: "json",
                success: success,
                error: error
            });
        },
        Fn_Message: function (id, type, message) {
            let result = '';
            switch (type) {
                case "s":
                    result = '<p class="alert alert-success">' + message + '</p>';
                    break;
                case "e":
                    result = '<p class="alert alert-danger">' + message + '</p>';
                    break;
                case "i":
                    result = '<p class="alert alert-warning">' + message + '</p>';
                    break;
            }
            $('div[id$=' + id + ']').empty().fadeIn().append(result);
            $('div[id$=' + id + ']').delay("20000").fadeOut();
            $('html, body').animate({ scrollTop: $('div[id$=' + id + ']').offset().top }, 'fast');
        }
    };

    return base;
}
