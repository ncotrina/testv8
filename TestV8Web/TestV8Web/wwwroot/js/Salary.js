﻿'use strict';
import { baseJs } from "/js/Base.js";
const base = baseJs();
let salary;
let listSaveMasive = [];
$(function () {
    salary = new Salary("/Home/");
    salary.Fn_Start();
});
class Salary {
    constructor(pageName) {
        this.salaryGetByEmployeeCode = pageName + 'GetByEmployeeCode';
        this.salaryGetByFilters = pageName + 'GetByFilters';
        this.salarySaveMasive = pageName + 'SaveMasive';
        this.salaryGetAll = pageName + 'GetAll';
    }
    Fn_Start() {
        salary.Fn_Bind();
        salary.Fn_Init();
    }
    Fn_Init() {
        $("#btnLoadAll").click();
    }
    Fn_Bind() {
        $("#btnSearchEmployeeCode").click(function (e) {
            e.preventDefault();
            let codeEmployee = $("input[id$='txtEmployeeCode']").val();
            if (codeEmployee === "") {
                base.Fn_Message('message_row', "i", "Debe ingresar un codigo de empleado");
            } else {
                let objData =
                {
                    EmployeeCode: codeEmployee
                };
                salary.Fn_LoadSalaries(objData, salary.salaryGetByEmployeeCode, true);
            }
        });
        $(".searchFilters").click(function (e) {
            e.preventDefault();
            let typeFilter = $(this).data('typefilter');
            let valueGrade = $("input[id$='txtGrade" + typeFilter + "']").val();
            if (valueGrade === "" || valueGrade === 0) {
                base.Fn_Message('message_row', "i", "Debe ingresar un grado valido. Rango ( 0 - 20 )");
            } else {
                let objData =
                {
                    Grade: valueGrade,
                    FilterType: typeFilter,
                    Position: $("select[id$='ddlPosicion']").val(),
                    Division: $("select[id$='ddlDivision']").val(),
                    Office: $("select[id$='ddlOffice']").val()
                };
                salary.Fn_LoadSalaries(objData, salary.salaryGetByFilters);
            }
        });       
        $("#btnLoadAll").click(function (e) {
            e.preventDefault();
            salary.Fn_LoadSalaries('{}', salary.salaryGetAll);
        });

        $("#btnAgregar").click(function (e) {
            e.preventDefault();
            let objData = [];
            let elementsInput = document.querySelectorAll(".form-control");
            let count = $("#tbSalary >tbody tr").length;
            objData.push({ index: count });
            elementsInput.forEach(element => {
                let field = $(element).data('class');
                if ($(element)[0].localName === "select") {
                    let valueDdl = $(element).find(" option:selected").text();
                    objData[0][field] = { Id: $(element).val(), Name: valueDdl }
                } else {
                    objData[0][field] = $(element).val();
                }
            });
            $("#tbSalary >tbody").append(base.Fn_LoadHandlebarTemplate("template-registermasive", { response: objData }));
            listSaveMasive[count] = objData[0];
            $('#btnSaveMasive').show();
            //salary.Fn_CleanControls()
            base.Fn_Message('message_row', "s", "Se agrego correctamente!");
        });
        $("#btnSaveMasive").click(function (e) {
            e.preventDefault();
            salary.Fn_SaveMasive();
        });
        $("body").on("click", ".delete-row", function (e) {
            e.preventDefault();
            listSaveMasive.splice($(this).closest('tr').index(), 1);
            $(this).closest('tr').remove();
            if (listSaveMasive.length === 0) $('#btnSaveMasive').hide();
        });
        $("body").on("click", ".edit-row", function (e) {
            e.preventDefault();
            let row = $(this).closest('tr');
            row.find('td').map(function () {
                let classData = $(this).data('class');
                let dataId = $(this).data('id');
                if (classData !== undefined && dataId === undefined) {
                    $('input[id$=txt' + classData + ']').val($(this).text());
                } else if (classData !== undefined && dataId !== undefined) {
                    $('select[id$=ddl' + classData + ']').val(dataId).trigger('change');
                }
            });
        });
    }

    Fn_LoadSalaries(objData, method, isBonus = false) {
        let success = function (response) {
            if (response.success) {
                $("#tbFilters >tbody").empty()
                $("#tbFilters >tbody").append(base.Fn_LoadHandlebarTemplate("template-filters", { response: response.dataList }));
                if (isBonus) base.Fn_Message('message_row', "s", response.message);
            } else { base.Fn_Message('message_row', "e", response.message); }
        };
        let error = function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            base.Fn_Message('message_row', "e", "Ocurrió un error inesperado!!");
        };
        base.Fn_CallMethod(method, objData, success, error, 'GET');
    }

    Fn_SaveMasive() {
        let success = function (response) {
            if (response.success) {
                $("#tbSalary >tbody").empty()
                $('#btnSaveMasive').hide();
                listSaveMasive = [];
                salary.Fn_CleanControls();
                base.Fn_Message('message_row', "s", response.message);
            } else {
                try {
                    let messageError = "";
                    let errorsValidations = JSON.parse(response.data).errors;
                    $.each(errorsValidations, function (indice, elemento) {
                       // $.each(elemento, function (indice2, elemento2) {
                            let text = parseInt(indice.match(/\[(.*)\]/).pop()) + 1;
                            let fila = "Fila " + text;
                            messageError += fila + " : " + elemento +"<br />";
                        //})
                    })
                    base.Fn_Message('message_row', "e", messageError);
                } catch (e) {
                    base.Fn_Message('message_row', "e", response.message);
                }
            }
        };
        let error = function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            base.Fn_Message('message_row', "e", "Ocurrió un error inesperado!!");
        };
        base.Fn_CallMethodPost(salary.salarySaveMasive, JSON.stringify(listSaveMasive), success, error);
    }
    Fn_CleanControls() {
        let elementsInput = document.querySelectorAll(".form-control");
        elementsInput.forEach(element => {
            if ($(element)[0].localName !== "select") {
                $(element).val("");
            }
        });
    }
}
