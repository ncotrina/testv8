# EXAMEN V8 -  EN .NET CORE #

Este proyecto web se ejecuta sobre ASP NET Core con EntityFrameworkCore.

### Requerimentos ###
- Visual Studio 2019
- SQL Server
- IIS Express

### Cadena Conexion SQL Server ###

```
Data Source=(localdb)\\.;Initial Catalog=TestV8Salary_BDTest;Integrated Security=true
```

### Instrucciones ###

1. Eliminar la carpeta de migración "TestV8API\Salaries.DataBase\Migrations"
2. Ejecutar los siguientes comandos, para crear la Bd y generar migraciones nuevamente:
	add-migration Initialize
	update-database
3. Verificar el nombre de la cadena de conexion (puede cambiarla)	
4. Abrir solución (.sln) con Visual Studio tanto para Backend y Frontend
5. Si hubiera algun problema con los nugets, restaurarlos desde la solucion de los proyectos
6. Verificar la url de los endpoints en el proyecto de Frontend, ubiquelo en appsetings.json - "Domain_ApiSalary"
