﻿using AutoMapper;
using Moq;
using Salaries.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Salary.Api.Test
{
    public class BaseTest
    {
        protected IMapper ConfiguraionMapper()
        {
            var config = new MapperConfiguration(options =>
              {
                  options.AddProfile(new Salaries.Core.Mappers.Mapper());
              });
            return config.CreateMapper();
        }

    }
    public class SalaryRepositoryMock
    {
        public Mock<ISalaryRepository> _salaryRepository { get; set; }
        public SalaryRepositoryMock()
        {
            _salaryRepository = new Mock<ISalaryRepository>();
        }
        private void Setup()
        {

        }

    }

}
