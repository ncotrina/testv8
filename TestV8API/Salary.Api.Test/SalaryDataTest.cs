﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Salary.Api.Test
{
    public static class SalaryDataTest
    {
        public static Salaries.Entity.Salary salary1 = new Salaries.Entity.Salary
        {
            EmployeeCode = "0100000066",
            EmployeeName = "Name 66",
            EmployeeSurname = " SurName 66",
            Division = new Salaries.Entity.Division
            {
                Id = 3,
                Name = "MARKETING"
            },
            Position = new Salaries.Entity.Position
            {
                Id = 6,
                Name = "MARKETING ASSISTANT"
            },
            BeginDate = Convert.ToDateTime("2021-01-01"),
            Birthday = Convert.ToDateTime("1986-01-20"),
            IdentificationNumber = "0202161266",
            BaseSalary = 5000,
            ProductionBonus = 100,
            CompensationBonus = 10,
            Commission = 200,
            Contributions = 10,
            Year = 2021,
            Month = 6,
            Grade = 5,
            Id = 1,
            Office = new Salaries.Entity.Office
            {
                Id = 1
            }
        };
        public static Salaries.Entity.Salary salary2 = new Salaries.Entity.Salary
        {

            EmployeeCode = "0100000066",
            EmployeeName = "Name 66",
            EmployeeSurname = " SurName 66",
            Division = new Salaries.Entity.Division
            {
                Id = 3,
                Name = "MARKETING"
            },
            Position = new Salaries.Entity.Position
            {
                Id = 6,
                Name = "MARKETING ASSISTANT"
            },
            BeginDate = Convert.ToDateTime("2021-01-01"),
            Birthday = Convert.ToDateTime("1986-01-20"),
            IdentificationNumber = "0202161266",
            BaseSalary = 5000,
            ProductionBonus = 100,
            CompensationBonus = 10,
            Commission = 200,
            Contributions = 10,
            Year = 2021,
            Month = 5,
            Grade = 4,
            Id = 2,
            Office = new Salaries.Entity.Office
            {
                Id = 3
            }
        };
        public static Salaries.Entity.Salary salary3 = new Salaries.Entity.Salary
        {
            EmployeeCode = "0100000066",
            EmployeeName = "Name 66",
            EmployeeSurname = " SurName 66",
            Division = new Salaries.Entity.Division
            {
                Id = 2,
                Name = "SALES"
            },
            Position = new Salaries.Entity.Position
            {
                Id = 4,
                Name = "SALES MANAGER"
            },
            BeginDate = Convert.ToDateTime("2021-01-01"),
            Birthday = Convert.ToDateTime("1986-01-20"),
            IdentificationNumber = "0202161266",
            BaseSalary = 1800,
            ProductionBonus = 33,
            CompensationBonus = 110,
            Commission = 120,
            Contributions = 110,
            Year = 2021,
            Month = 4,
            Grade = 8,
            Id = 3,
            Office = new Salaries.Entity.Office
            {
                Id = 3
            }
        };
        public static Salaries.Entity.Salary salary4 = new Salaries.Entity.Salary
        {
            EmployeeCode = "0100000077",
            EmployeeName = "Name 77",
            EmployeeSurname = " SurName 77",
            Division = new Salaries.Entity.Division
            {
                Id = 3,
                Name = "MARKETING"
            },
            Position = new Salaries.Entity.Position
            {
                Id = 6,
                Name = "MARKETING ASSISTANT"
            },
            BeginDate = Convert.ToDateTime("2021-01-01"),
            Birthday = Convert.ToDateTime("1986-01-20"),
            IdentificationNumber = "0202161277",
            BaseSalary = 4200,
            ProductionBonus = 150,
            CompensationBonus = 10,
            Commission = 120,
            Contributions = 20,
            Year = 2021,
            Month = 6,
            Grade = 6,
            Id = 4,
            Office = new Salaries.Entity.Office
            {
                Id = 3
            }
        };
        public static Salaries.Entity.Salary salary5 = new Salaries.Entity.Salary
        {

            EmployeeCode = "0100000077",
            EmployeeName = "Name 77",
            EmployeeSurname = " SurName 77",
            Division = new Salaries.Entity.Division
            {
                Id = 3,
                Name = "MARKETING"
            },
            Position = new Salaries.Entity.Position
            {
                Id = 6,
                Name = "MARKETING ASSISTANT"
            },
            BeginDate = Convert.ToDateTime("2021-01-01"),
            Birthday = Convert.ToDateTime("1986-01-20"),
            IdentificationNumber = "0202161277",
            BaseSalary = 5000,
            ProductionBonus = 100,
            CompensationBonus = 10,
            Commission = 200,
            Contributions = 10,
            Year = 2021,
            Month = 5,
            Grade = 9,
            Id = 5,
            Office = new Salaries.Entity.Office
            {
                Id = 2
            }
        };
        public static Salaries.Entity.Salary salary6 = new Salaries.Entity.Salary
        {
            EmployeeCode = "0100000077",
            EmployeeName = "Name 77",
            EmployeeSurname = " SurName 77",
            Division = new Salaries.Entity.Division
            {
                Id = 4,
                Name = "Test"
            },
            Position = new Salaries.Entity.Position
            {
                Id = 2,
                Name = "SALES Test"
            },
            BeginDate = Convert.ToDateTime("2021-01-01"),
            Birthday = Convert.ToDateTime("1986-01-20"),
            IdentificationNumber = "0202161277",
            BaseSalary = 500,
            ProductionBonus = 100,
            CompensationBonus = 200,
            Commission = 120,
            Contributions = 110,
            Year = 2021,
            Month = 4,
            Grade = 7,
            Id = 6,
            Office = new Salaries.Entity.Office
            {
                Id = 4
            }
        };
        public static Salaries.Entity.Salary salary7 = new Salaries.Entity.Salary
        {
            EmployeeCode = "0100000088",
            EmployeeName = "Name 88",
            EmployeeSurname = " SurName 88",
            Division = new Salaries.Entity.Division
            {
                Id = 3,
                Name = "Test"
            },
            Position = new Salaries.Entity.Position
            {
                Id = 1,
                Name = "SALES Test"
            },
            BeginDate = Convert.ToDateTime("2021-01-01"),
            Birthday = Convert.ToDateTime("1986-01-20"),
            IdentificationNumber = "0202161288",
            BaseSalary = 1500,
            ProductionBonus = 10,
            CompensationBonus = 20,
            Commission = 12,
            Contributions = 10,
            Year = 2021,
            Month = 4,
            Grade = 12,
            Id = 7,
            Office = new Salaries.Entity.Office
            {
                Id = 1
            }
        };
        public static Salaries.Entity.Salary salary8 = new Salaries.Entity.Salary
        {
            EmployeeCode = "0100000099",
            EmployeeName = "Name 99",
            EmployeeSurname = " SurName 99",
            Division = new Salaries.Entity.Division
            {
                Id = 2,
                Name = "Test"
            },
            Position = new Salaries.Entity.Position
            {
                Id = 2,
                Name = "SALES Test"
            },
            BeginDate = Convert.ToDateTime("2021-01-01"),
            Birthday = Convert.ToDateTime("1986-01-20"),
            IdentificationNumber = "0202161299",
            BaseSalary = 2800,
            ProductionBonus = 30,
            CompensationBonus = 20,
            Commission = 50,
            Contributions = 10,
            Year = 2021,
            Month = 4,
            Grade = 5,
            Id = 8,
            Office = new Salaries.Entity.Office
            {
                Id = 3
            }
        };
        public static Salaries.Entity.Salary salary10 = new Salaries.Entity.Salary
        {
            EmployeeCode = "0100000011",
            EmployeeName = "Name 11",
            EmployeeSurname = " SurName 11",
            Division = new Salaries.Entity.Division
            {
                Id = 2,
                Name = "MARKETING ddd"
            },
            Position = new Salaries.Entity.Position
            {
                Id = 5,
                Name = "MARKETING ddd"
            },
            BeginDate = Convert.ToDateTime("2021-01-01"),
            Birthday = Convert.ToDateTime("1986-01-20"),
            IdentificationNumber = "0202161211",
            BaseSalary = 4500,
            ProductionBonus = 100,
            CompensationBonus = 10,
            Commission = 200,
            Contributions = 10,
            Year = 2021,
            Month = 6,
            Grade = 5,
            Id = 10,
            Office = new Salaries.Entity.Office
            {
                Id = 3
            }
        };
        public static Salaries.Entity.Salary salary11 = new Salaries.Entity.Salary
        {

            EmployeeCode = "0100000011",
            EmployeeName = "Name 11",
            EmployeeSurname = " SurName 11",
            Division = new Salaries.Entity.Division
            {
                Id = 4,
                Name = "MARKEdddTING"
            },
            Position = new Salaries.Entity.Position
            {
                Id = 3,
                Name = "MARKETING dd"
            },
            BeginDate = Convert.ToDateTime("2021-01-01"),
            Birthday = Convert.ToDateTime("1986-01-20"),
            IdentificationNumber = "0202161266",
            BaseSalary = 5000,
            ProductionBonus = 100,
            CompensationBonus = 10,
            Commission = 200,
            Contributions = 10,
            Year = 2021,
            Month = 5,
            Grade = 3,
            Id = 11,
            Office = new Salaries.Entity.Office
            {
                Id = 1
            }
        };
        public static Salaries.Entity.Salary salary12 = new Salaries.Entity.Salary
        {
            EmployeeCode = "0100000011",
            EmployeeName = "Name 11",
            EmployeeSurname = " SurName 11",
            Division = new Salaries.Entity.Division
            {
                Id = 1,
                Name = "ssss"
            },
            Position = new Salaries.Entity.Position
            {
                Id = 1,
                Name = "aaa"
            },
            BeginDate = Convert.ToDateTime("2021-01-01"),
            Birthday = Convert.ToDateTime("1986-01-20"),
            IdentificationNumber = "0202161211",
            BaseSalary = 1800,
            ProductionBonus = 33,
            CompensationBonus = 110,
            Commission = 120,
            Contributions = 110,
            Year = 2021,
            Month = 4,
            Grade = 6,
            Id = 12,
            Office = new Salaries.Entity.Office
            {
                Id = 3
            }
        };
        public static List<Salaries.Entity.Salary> listSalary = new List<Salaries.Entity.Salary>(new Salaries.Entity.Salary[] {
            salary1,salary2,salary3,salary4,salary5,salary6,salary7,salary8, salary10,salary11,salary12
        });
        public static List<Salaries.Entity.Salary> listSalaryRepeat = new List<Salaries.Entity.Salary>(new Salaries.Entity.Salary[] {
            salary1,salary2,salary2,salary2,salary3
        });
    }
}
