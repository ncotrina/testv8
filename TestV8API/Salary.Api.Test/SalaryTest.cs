using AutoMapper;
using Moq;
using NUnit.Framework;
using Salaries.Core.DTOs;
using Salaries.Core.Interfaces;
using Salaries.Core.Services;
using Salaries.Core.Utilities;
using Salaries.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Salary.Api.Test
{
    public class SalaryTest : BaseTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task GetByEmployeeNotExistTest()
        {
            SalaryFilterDto salaryFilter = new SalaryFilterDto { EmployeeCode = "01000000" };
            var mapper = ConfiguraionMapper();
            Mock<ISalaryRepository> mock = new Mock<ISalaryRepository>();
            mock.Setup(x => x.GetByEmployee(salaryFilter.EmployeeCode)).ReturnsAsync(SalaryDataTest.listSalary.Where(x => x.EmployeeCode == salaryFilter.EmployeeCode).AsQueryable());
            SalaryService service = new SalaryService(mock.Object, mapper);
            var data = await service.GetByEmployee(salaryFilter);
            Assert.AreEqual("No existen registros para el empleado ingresado", data.Message);
        }
        [Test]
        public async Task GetByEmployeeExistTest()
        {
            SalaryFilterDto salaryFilter = new SalaryFilterDto { EmployeeCode = "0100000066" };
            var mapper = ConfiguraionMapper();
            Mock<ISalaryRepository> mock = new Mock<ISalaryRepository>();
            mock.Setup(x => x.GetByEmployee(salaryFilter.EmployeeCode)).ReturnsAsync(SalaryDataTest.listSalary.Where(x => x.EmployeeCode == salaryFilter.EmployeeCode).AsQueryable());
            SalaryService service = new SalaryService(mock.Object, mapper);
            var data = await service.GetByEmployee(salaryFilter);
            Assert.IsTrue(data.Success);
        }

        [Test]
        public async Task SalarySaveMasiveSuccessTest()
        {
            var mapper = ConfiguraionMapper();
            Mock<ISalaryRepository> mock = new Mock<ISalaryRepository>();
            mock.Setup(x => x.InsertMasive(SalaryDataTest.listSalary.AsEnumerable()));
            SalaryService service = new SalaryService(mock.Object, mapper);
            var mapListDto2 = mapper.Map<IList<SalaryDto>>(SalaryDataTest.listSalary);
            SalaryListDto salaryListDto = new SalaryListDto
            {
                SalaryListsDto = mapListDto2
            };
            var data = await service.InsertMasive(salaryListDto);
            Assert.IsTrue(data.Success);
            Assert.AreEqual("Los salarios se guardaron correctamente!", data.Message);
        }
        [Test]
        public async Task SalarySaveMasiveRepeatMonthYearEmployeeTest()
        {
            var mapper = ConfiguraionMapper();
            Mock<ISalaryRepository> mock = new Mock<ISalaryRepository>();
            mock.Setup(x => x.InsertMasive(SalaryDataTest.listSalaryRepeat.AsEnumerable()));
            SalaryService service = new SalaryService(mock.Object, mapper);
            var mapListDto2 = mapper.Map<IList<SalaryDto>>(SalaryDataTest.listSalaryRepeat);
            SalaryListDto salaryListDto = new SalaryListDto
            {
                SalaryListsDto = mapListDto2
            };
            var data = await service.InsertMasive(salaryListDto);
            Assert.IsTrue(!data.Success);
            Assert.AreEqual("Existen empleados que contienen informaci�n repetida, mes y a�o deben ser unicos!", data.Message);
        }
    }

}