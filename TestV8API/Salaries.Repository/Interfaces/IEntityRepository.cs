﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Salaries.Repository.Interfaces
{
    public interface IEntityRepository<T>
    {
        Task<IEnumerable<T>> GetAll();
    }
}
