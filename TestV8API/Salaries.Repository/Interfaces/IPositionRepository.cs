﻿using Salaries.Entity;

namespace Salaries.Repository.Interfaces
{
    public interface IPositionRepository : IEntityRepository<Position>
    {
    }
}
