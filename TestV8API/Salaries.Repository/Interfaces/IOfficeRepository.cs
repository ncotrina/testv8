﻿using Salaries.Entity;

namespace Salaries.Repository.Interfaces
{
    public interface IOfficeRepository : IEntityRepository<Office>
    {
    }
}
