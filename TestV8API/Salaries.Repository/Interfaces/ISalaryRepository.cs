﻿using Salaries.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Salaries.Repository.Interfaces
{
    public interface ISalaryRepository : IEntityRepository<Salary>
    {
        Task InsertMasive(IEnumerable<Salary> salaries);
        IQueryable<Salary> GetByFilters();
        Task<IEnumerable<Salary>> GetByEmployee(String employee);
        Task<Boolean> ValidateDuplicate(Salary salary);
        Task<Boolean> ValidateEmployeeFullName(Salary salary);
    }
}
