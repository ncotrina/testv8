﻿using Microsoft.EntityFrameworkCore;
using Salaries.DataBase;
using Salaries.Entity;
using Salaries.Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Salaries.Repository.Repositories
{
    public class DivisionRepository : IDivisionRepository
    {
        private readonly ApplicationDbContext _context;
        public DivisionRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Division>> GetAll()
        {
           return await _context.Divisions.ToListAsync();
        }
    }
}
