﻿using Microsoft.EntityFrameworkCore;
using Salaries.DataBase;
using Salaries.Entity;
using Salaries.Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Salaries.Repository.Repositories
{
    public class PositionRepository : IPositionRepository
    {
        private readonly ApplicationDbContext _context;
        public PositionRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Position>> GetAll()
        {
            return await _context.Positions.ToListAsync();
        }
    }
}
