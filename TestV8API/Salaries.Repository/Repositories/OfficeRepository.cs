﻿using Microsoft.EntityFrameworkCore;
using Salaries.DataBase;
using Salaries.Entity;
using Salaries.Repository.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Salaries.Repository.Repositories
{
    public class OfficeRepository : IOfficeRepository
    {
        private readonly ApplicationDbContext _context;
        public OfficeRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Office>> GetAll()
        {
            return await _context.Offices.ToListAsync();
        }
    }
}
