﻿using Microsoft.EntityFrameworkCore;
using Salaries.DataBase;
using Salaries.Entity;
using Salaries.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Salaries.Repository.Repositories
{
    public class SalaryRepository : ISalaryRepository
    {
        private readonly ApplicationDbContext _context;
        public SalaryRepository(ApplicationDbContext dbContext)
        {
            _context = dbContext;
        }

        public async Task<IEnumerable<Salary>> GetByEmployee(String employee)
        {
            return await _context.Salaries.Where(x => x.EmployeeCode.Equals(employee))
                                    .Include(x => x.Division)
                                    .Include(x => x.Position)
                                    .ToListAsync();
        }

        public IQueryable<Salary> GetByFilters()
        {
            return _context.Salaries.Include(x => x.Division)
                                    .Include(x => x.Position)
                                    .Include(x => x.Office)
                                    .AsQueryable();
        }
        public async Task InsertMasive(IEnumerable<Salary> salaries)
        {
            await _context.Salaries.AddRangeAsync(salaries);
            await _context.SaveChangesAsync();
        }

        public async Task<Boolean> ValidateDuplicate(Salary salary)
        {
            return await _context.Salaries.AnyAsync(x => x.EmployeeCode == salary.EmployeeCode && x.Month == salary.Month && x.Year == salary.Year);
        }

        public async Task<Boolean> ValidateEmployeeFullName(Salary salary)
        {
            return await _context.Salaries.AnyAsync(x => x.EmployeeName == salary.EmployeeName && x.EmployeeSurname == salary.EmployeeSurname && x.IdentificationNumber == salary.IdentificationNumber);
        }

        public async Task<IEnumerable<Salary>> GetAll()
        {
            return await _context.Salaries.Include(x => x.Division)
                                    .Include(x => x.Position).Include(x => x.Office)
                                    .ToListAsync();
        }
    }
}
