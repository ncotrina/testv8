﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Salaries.Entity
{
    public class Salary
    {
        public Int32 Id { get; set; }
        public Int32 Year { get; set; }
        public Int32 Month { get; set; }
        public String EmployeeCode { get; set; }
        public String EmployeeName { get; set; }
        public String EmployeeSurname { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime Birthday { get; set; }
        public String IdentificationNumber { get; set; }
        public Int32 Grade { get; set; }
        public Decimal BaseSalary { get; set; }
        public Decimal ProductionBonus { get; set; }
        public Decimal CompensationBonus { get; set; }
        public Decimal Commission { get; set; }
        public Decimal Contributions { get; set; }
        public Int16 OfficeId { get; set; }
        public virtual Office Office { get; set; }
        public  Int16 PositionId { get; set; }
        public virtual Position Position { get; set; }
        public Int16 DivisionId { get; set; }
        public virtual Division Division { get; set; }
        [NotMapped]
        public Decimal TotalSalary { get { return CalculateSalary(); } }
        private Decimal CalculateSalary()
        {
            Decimal otherIncome = (BaseSalary + Commission) * (Decimal)0.08 + Commission;
            Decimal totalSalary = BaseSalary + ProductionBonus + (CompensationBonus * (Decimal)0.75) + otherIncome - Contributions;
            return totalSalary;
        }
    }
}
