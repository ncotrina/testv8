﻿using System;

namespace Salaries.Entity
{
    public class BaseEntity
    {
        public Int16 Id { get; set; }
        public String Name { get; set; }
    }
}
