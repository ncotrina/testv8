﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Salaries.Entity;
using System.Collections.Generic;

namespace Salaries.DataBase.Config
{
    public class PositionConfig 
    {
        public PositionConfig(EntityTypeBuilder<Position> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasColumnName("PositionId");
            builder.Property(e => e.Name).HasMaxLength(50).IsRequired();
            #region Insert Data Default
            List<Position> position = new List<Position>
            {
                new Position
                {
                    Id = 1,
                    Name = "CARGO MANAGER"
                },
                new Position
                {
                    Id = 2,
                    Name = "HEAD OF CARGO"
                },
                new Position
                {
                    Id = 3,
                    Name = "CARGO ASSISTANT"
                },
                new Position
                {
                    Id = 4,
                    Name = "SALES MANAGER"
                },
                new Position
                {
                    Id = 5,
                    Name = "ACCOUNT EXECUTIVE"
                },
                new Position
                {
                    Id = 6,
                    Name = "MARKETING ASSISTANT"
                },
                new Position
                {
                    Id = 7,
                    Name = "CUSTOMER DIRECTOR"
                },
                new Position
                {
                    Id = 8,
                    Name = "CUSTOMER ASSISTANT"
                }
            };
            builder.HasData(position);
            #endregion
        }
    }
}
