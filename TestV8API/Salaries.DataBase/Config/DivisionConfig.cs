﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Salaries.Entity;
using System.Collections.Generic;

namespace Salaries.DataBase.Config
{
    public class DivisionConfig
    {
        public DivisionConfig(EntityTypeBuilder<Division> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasColumnName("DivisionId");
            builder.Property(e => e.Name).HasMaxLength(50).IsRequired();

            #region Insert Data Default
            List<Division> division = new List<Division>
            {
                new Division
                {
                    Id = 1,
                    Name = "OPERATION"
                },
                new Division
                {
                    Id = 2,
                    Name = "SALES"
                },
                new Division
                {
                    Id = 3,
                    Name = "MARKETING"
                },
                new Division
                {
                    Id = 4,
                    Name = "CUSTOMER CARE"
                }
            };
            builder.HasData(division);
            #endregion
        }
    }
}
