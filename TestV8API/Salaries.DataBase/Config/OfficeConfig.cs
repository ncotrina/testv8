﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Salaries.Entity;
using System.Collections.Generic;

namespace Salaries.DataBase.Config
{
    public class OfficeConfig
    {
        public OfficeConfig(EntityTypeBuilder<Office> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasColumnName("OfficeId");
            builder.Property(e => e.Name).HasMaxLength(50).IsRequired();

            #region Insert Data Default
            List<Office> office = new List<Office>
            {
                new Office
                {
                    Id = 1,
                    Name = "A"
                },
                new Office
                {
                    Id = 2,
                    Name = "C"
                },
                new Office
                {
                    Id = 3,
                    Name = "D"
                },
                new Office
                {
                    Id = 4,
                    Name = "ZZ"
                }
            };
            builder.HasData(office);
            #endregion
        }
    }
}
