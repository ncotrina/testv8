﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Salaries.Entity;
using System;
using System.Collections.Generic;

namespace Salaries.DataBase.Config
{
    public class SalaryConfig
    {
        public SalaryConfig(EntityTypeBuilder<Salary> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id)
                .HasColumnName("SalaryId");

            builder.Property(e => e.EmployeeCode)
                .HasMaxLength(10)
                .IsRequired();

            builder.Property(e => e.EmployeeName)
                .HasMaxLength(150)
                .IsRequired();

            builder.Property(e => e.EmployeeSurname)
                .HasMaxLength(150)
                .IsRequired();

            builder.Property(e => e.BeginDate)
               .HasColumnType("date")
               .IsRequired();

            builder.Property(e => e.Birthday)
                .HasColumnType("date")
                .IsRequired();

            builder.Property(e => e.IdentificationNumber)
                .HasMaxLength(10)
                .IsRequired();

            builder.Property(e => e.BaseSalary)
                .HasColumnType("decimal(18,2)")
                .IsRequired();

            builder.Property(e => e.ProductionBonus)
                .HasColumnType("decimal(18,2)")
                .IsRequired();

            builder.Property(e => e.CompensationBonus)
                .HasColumnType("decimal(18,2)")
                .IsRequired();

            builder.Property(e => e.Commission)
                .HasColumnType("decimal(18,2)")
                .IsRequired();

            builder.Property(e => e.Contributions)
                .HasColumnType("decimal(18,2)")
                .IsRequired();


            #region Insert Data
            List<Salary> salary = new List<Salary>();
            Random random = new Random();
            String identificatorInitial = "2021612";
            String employeeCode = "1000000";
            Int32 id = 1;
            Int32 count = 0;
            DateTime beginDate = Convert.ToDateTime("2021-01-01");
            DateTime birthDay = Convert.ToDateTime("1980-01-01");
            for (Int32 j = 1; j <= 100; j++)
            {
                for (Int16 i = 1; i <= 6; i++)
                {
                    if (i == 1) { count++; birthDay = birthDay.AddDays(count); }
                    String identificatorNumber = identificatorInitial + count;
                    String identificatorCode = employeeCode + count;

                    salary.Add(new Salary
                    {
                        Id = id,
                        Year = DateTime.Now.Year,
                        Month = i,
                        OfficeId = Convert.ToInt16(random.Next(1, 4)),

                        EmployeeCode = Convert.ToInt64(identificatorCode).ToString("D10"),
                        EmployeeName = $"Name {count}",
                        EmployeeSurname = $"SurName {count}",
                        BeginDate = beginDate,
                        Birthday = birthDay,
                        IdentificationNumber = Convert.ToInt64(identificatorNumber).ToString("D10"),

                        DivisionId = Convert.ToInt16(random.Next(1, 4)),
                        PositionId = Convert.ToInt16(random.Next(1, 8)),
                        Grade = random.Next(1, 20),
                        BaseSalary = random.Next(1500, 5000),
                        ProductionBonus = random.Next(0, 2000),
                        CompensationBonus = random.Next(0, 2000),
                        Commission = random.Next(0, 2000),
                        Contributions = random.Next(0, 2000)
                    });
                    id++;

                }
            }
            builder.HasData(salary);
            #endregion
        }
    }
}
