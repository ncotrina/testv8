﻿using Microsoft.EntityFrameworkCore;
using Salaries.DataBase.Config;
using Salaries.Entity;

namespace Salaries.DataBase
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        public DbSet<Salary> Salaries { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Division> Divisions { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            ModelConfig(builder);
        }
        private void ModelConfig(ModelBuilder builder)
        {
            _ = new SalaryConfig(builder.Entity<Salary>());
            _ = new OfficeConfig(builder.Entity<Office>());
            _ = new PositionConfig(builder.Entity<Position>());
            _ = new DivisionConfig(builder.Entity<Division>());
        }
    }
}
