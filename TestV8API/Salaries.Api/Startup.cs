using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Salaries.Api.Filters;
using Salaries.Core.Interfaces;
using Salaries.Core.Services;
using Salaries.DataBase;
using Salaries.Repository.Interfaces;
using Salaries.Repository.Repositories;
using System;

namespace Salaries.Api
{
    /// <summary>
    /// Class Startup
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Constructor Startup
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        /// <summary>
        /// Interface Configuration
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            #region Configurations
            #region Mappers
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            #endregion Mappers
            #region DataBase
            services.AddDbContext<ApplicationDbContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("DataBaseConnection")
            ));
            #endregion DataBase
            #region Services
            services.AddTransient<ISalaryService, SalaryService>();
            services.AddTransient<IPositionService, PositionService>();
            services.AddTransient<IOfficeService, OfficeService>();
            services.AddTransient<IDivisionService, DivisionService>();
            #endregion Services
            #region Repositories
            services.AddTransient<ISalaryRepository, SalaryRepository>();
            services.AddTransient<IPositionRepository, PositionRepository>();
            services.AddTransient<IDivisionRepository, DivisionRepository>();
            services.AddTransient<IOfficeRepository, OfficeRepository>();
            #endregion Repositories
            services.AddTransient<ValidateModelAttribute>();
            #region Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Contact = new Microsoft.OpenApi.Models.OpenApiContact { Email = "nicky_17_11@hotmail.com", Name = "Nicky Alejandro Cotrina de la Cruz" },
                    Description = "Services V8",
                    Title = "Services V8",
                    Version = "v1.0"
                });
                String pathXml = AppDomain.CurrentDomain.BaseDirectory + @"Salaries.Api.xml";
                c.IncludeXmlComments(pathXml);
            });
            #endregion Swagger
            #endregion Configurations
            services.AddControllers(); 
            services.AddCors(opciones =>
            {
                opciones.AddDefaultPolicy(builder =>
                {
                    builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
                });
            });
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "CoreAPI");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
