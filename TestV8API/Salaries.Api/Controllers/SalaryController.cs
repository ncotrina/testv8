﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Salaries.Api.Filters;
using Salaries.Core.DTOs;
using Salaries.Core.Interfaces;
using Salaries.Core.Utilities;

namespace Salaries.Api.Controllers
{
    /// <summary>
    /// Controller Salary
    /// </summary>

    [Route("api/[controller]")]
    [ApiController]
    public class SalaryController : ControllerBase
    {
        private readonly ISalaryService _salaryService;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="salaryService"></param>
        public SalaryController(ISalaryService salaryService)
        {
            _salaryService = salaryService;
        }
        /// <summary>
        /// Insert masive an employees salary information
        /// </summary>
        /// <param name="salaryListDto"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(ValidateModelAttribute))]
        public async Task<ServiceResponse<SalaryDto>> PostMasive(SalaryListDto salaryListDto)
        {
            return await _salaryService.InsertMasive(salaryListDto);
        }

        /// <summary>
        /// Get Salaries by filters
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet("filters")]
        public ServiceResponse<SalaryEmployeeDto> Get([FromQuery]SalaryFilterDto filter)
        {
            return _salaryService.GetByFilters(filter);
        }
        /// <summary>
        /// Get Salaries by employee code
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet("employee")]
        public async Task<ServiceResponse<SalaryEmployeeDto>> GetByEmployeeCode([FromQuery]SalaryFilterDto filter)
        {
            return await _salaryService.GetByEmployee(filter);
        }
        /// <summary>
        /// Get All Salaries
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public async Task<ServiceResponse<SalaryEmployeeDto>> GetAll()
        {
            return await _salaryService.GetAll();
        }
    }
}