﻿using Microsoft.AspNetCore.Mvc;
using Salaries.Core.DTOs;
using Salaries.Core.Interfaces;
using Salaries.Core.Services;
using Salaries.Core.Utilities;
using System.Threading.Tasks;

namespace Salaries.Api.Controllers
{
    /// <summary>
    /// Controller Office
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class OfficeController : ControllerBase
    {
        private readonly IOfficeService _officeService;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="officeService"></param>
        public OfficeController(IOfficeService officeService)
        {
            _officeService = officeService;
        }
        /// <summary>
        /// Get All Offices
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public async Task<ServiceResponse<BaseEntityDto>> Get()
        {
            return await _officeService.GetAll();
        }
    }
}