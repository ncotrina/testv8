﻿using Microsoft.AspNetCore.Mvc;
using Salaries.Core.DTOs;
using Salaries.Core.Interfaces;
using Salaries.Core.Utilities;
using System.Threading.Tasks;

namespace Salaries.Api.Controllers
{
    /// <summary>
    /// Controller Position
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PositionController : ControllerBase
    {
        private readonly IPositionService _positionService;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="positionService"></param>
        public PositionController(IPositionService positionService)
        {
            _positionService = positionService;
        }
        /// <summary>
        /// Get All Position
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public async Task<ServiceResponse<BaseEntityDto>> Get()
        {
            return await _positionService.GetAll();
        }
    }
}