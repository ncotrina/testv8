﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Salaries.Core.DTOs;
using Salaries.Core.Interfaces;
using Salaries.Core.Services;
using Salaries.Core.Utilities;

namespace Salaries.Api.Controllers
{
    /// <summary>
    /// Controller Division
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class DivisionController : ControllerBase
    {
        private readonly IDivisionService _divisionService;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="divisionService"></param>
        public DivisionController(IDivisionService divisionService)
        {
            _divisionService = divisionService;
        }
        /// <summary>
        /// Get All Divisions
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public async Task<ServiceResponse<BaseEntityDto>> Get()
        {
            return await _divisionService.GetAll();
        }

    }
}