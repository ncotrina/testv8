﻿using AutoMapper;
using Salaries.Core.DTOs;
using Salaries.Core.Interfaces;
using Salaries.Core.Utilities;
using Salaries.Entity;
using Salaries.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Salaries.Core.Services
{
    public class DivisionService : IDivisionService
    {
        private readonly IDivisionRepository _divisionRepository;
        private readonly IMapper _mapper;

        public DivisionService(IDivisionRepository divisionRepository, IMapper mapper)
        {
            _divisionRepository = divisionRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResponse<BaseEntityDto>> GetAll()
        {
            ServiceResponse<BaseEntityDto> serviceResponse = new ServiceResponse<BaseEntityDto>();
            try
            {
                IEnumerable<Division> divisions = await _divisionRepository.GetAll();
                serviceResponse.DataList = _mapper.Map<IList<BaseEntityDto>>(divisions);
                serviceResponse.Success = true;
            }
            catch (Exception ex)
            {
                serviceResponse.Message = ex.Message;
            }
            return serviceResponse;
        }
    }
}
