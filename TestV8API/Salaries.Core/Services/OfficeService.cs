﻿using AutoMapper;
using Salaries.Core.DTOs;
using Salaries.Core.Interfaces;
using Salaries.Core.Utilities;
using Salaries.Entity;
using Salaries.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Salaries.Core.Services
{
    public class OfficeService : IOfficeService
    {
        private readonly IOfficeRepository _officeRepository;
        private readonly IMapper _mapper;

        public OfficeService(IOfficeRepository officeRepository, IMapper mapper)
        {
            _officeRepository = officeRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResponse<BaseEntityDto>> GetAll()
        {
            ServiceResponse<BaseEntityDto> serviceResponse = new ServiceResponse<BaseEntityDto>();
            try
            {
                IEnumerable<Office> divisions = await _officeRepository.GetAll();
                serviceResponse.DataList = _mapper.Map<IList<BaseEntityDto>>(divisions);
                serviceResponse.Success = true;
            }
            catch (Exception ex)
            {
                serviceResponse.Message = ex.Message;
            }
            return serviceResponse;
        }
    }
}
