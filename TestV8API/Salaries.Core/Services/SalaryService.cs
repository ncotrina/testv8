﻿using AutoMapper;
using Salaries.Core.DTOs;
using Salaries.Core.Interfaces;
using Salaries.Core.Utilities;
using Salaries.Entity;
using Salaries.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Salaries.Core.Services
{
    public class SalaryService : ISalaryService
    {
        private readonly ISalaryRepository _salaryRepository;
        private readonly IMapper _mapper;
        public SalaryService(ISalaryRepository salaryRepository, IMapper mapper)
        {
            _salaryRepository = salaryRepository;
            _mapper = mapper;
        }
        public async Task<ServiceResponse<SalaryDto>> InsertMasive(SalaryListDto salaryListDto)
        {
            ServiceResponse<SalaryDto> serviceResponse = new ServiceResponse<SalaryDto>();
            try
            {
                StringBuilder stringMessageError = new StringBuilder();
                Boolean validateRepeat = salaryListDto.SalaryListsDto.GroupBy(x => new { x.Year, x.Month, x.EmployeeCode }).Any(g => g.Count() > 1);
                if (!validateRepeat)
                {
                    await ValidateDuplicate(salaryListDto, stringMessageError);
                    if (!String.IsNullOrEmpty(stringMessageError.ToString()))
                    {
                        serviceResponse.Message = stringMessageError.ToString();
                    }
                    else
                    {
                        IList<Salary> salaries = _mapper.Map<IList<Salary>>(salaryListDto.SalaryListsDto);
                        await _salaryRepository.InsertMasive(salaries);
                        serviceResponse.Message = "Los salarios se guardaron correctamente!";
                        serviceResponse.Success = true;
                    }
                }
                else
                {
                    serviceResponse.Message = "Existen empleados que contienen información repetida, mes y año deben ser unicos!";
                }
            }
            catch (Exception ex)
            {
                serviceResponse.Message = ex.Message;
            }
            return serviceResponse;
        }
        private async Task ValidateDuplicate(SalaryListDto salaryListDto, StringBuilder stringMessageError)
        {
            foreach (SalaryDto item in salaryListDto.SalaryListsDto)
            {
                Salary salaryItem = _mapper.Map<Salary>(item);
                Boolean existDistributor = await _salaryRepository.ValidateEmployeeFullName(salaryItem);
                if (existDistributor)
                {
                    stringMessageError.Append($"El Empleado: {item.EmployeeCode}. YA FUE REGISTRADO ANTERIORMENTE CON EL NOMBRE:{item.EmployeeName} {item.EmployeeSurname}.");
                }
                else
                {
                    Boolean isDuplicate = await _salaryRepository.ValidateDuplicate(salaryItem);
                    if (isDuplicate)
                    {
                        stringMessageError.Append($"Empleado: {item.EmployeeCode}. YA FUE REGISTRADO ANTERIORMENTE CON EL Mes:{item.Month}. Año:{item.Year}. !!");
                    }
                }
            }
        }
        public ServiceResponse<SalaryEmployeeDto> GetByFilters(SalaryFilterDto filter)
        {
            ServiceResponse<SalaryEmployeeDto> serviceResponse = new ServiceResponse<SalaryEmployeeDto>();
            try
            {
                IQueryable<Salary> salaries = _salaryRepository.GetByFilters();
                if (filter.FilterType == 1)
                {
                    if (filter.Office == 0)
                    {
                        salaries = salaries.Where(x => x.Grade == filter.Grade);
                    }
                    else
                    {
                        salaries = salaries.Where(x => x.Office.Id == filter.Office && x.Grade == filter.Grade);
                    }
                }
                else if (filter.FilterType == 2)
                {
                    salaries = salaries.Where(x => x.Position.Id == filter.Position && x.Grade == filter.Grade);
                }
                else if (filter.FilterType == 3)
                {
                    salaries = salaries.Where(x => x.Division.Id == filter.Division && x.Grade == filter.Grade);
                }
                IList<SalaryDto> salariesMapper = _mapper.Map<IList<SalaryDto>>(salaries.ToList());
                serviceResponse.DataList = _mapper.Map<IList<SalaryEmployeeDto>>(salariesMapper);
                serviceResponse.Success = true;
            }
            catch (Exception ex)
            {
                serviceResponse.Message = ex.Message;
            }
            return serviceResponse;
        }
        public async Task<ServiceResponse<SalaryEmployeeDto>> GetByEmployee(SalaryFilterDto filter)
        {
            ServiceResponse<SalaryEmployeeDto> serviceResponse = new ServiceResponse<SalaryEmployeeDto>();
            try
            {
                IEnumerable<Salary> salaries = await _salaryRepository.GetByEmployee(filter.EmployeeCode);
                if (salaries != null && salaries.Count() > 0)
                {
                    List<Salary> salariesMonths = salaries.OrderByDescending(x => x.Year).ThenByDescending(x => x.Month).Take(3).ToList();
                    Decimal bonusCalculate = salariesMonths.Sum(x => x.TotalSalary) / 3;
                    IList<SalaryDto> salariesMapper = _mapper.Map<IList<SalaryDto>>(salariesMonths.ToList());
                    serviceResponse.Message = $"El bono del empleado es: {bonusCalculate}";
                    serviceResponse.DataList = _mapper.Map<IList<SalaryEmployeeDto>>(salariesMapper);
                    serviceResponse.Success = true;
                }
                else
                {
                    serviceResponse.Message = "No existen registros para el empleado ingresado";
                }
            }
            catch (Exception ex)
            {
                serviceResponse.Message = ex.Message;
            }
            return serviceResponse;
        }
        public async Task<ServiceResponse<SalaryEmployeeDto>> GetAll()
        {
            ServiceResponse<SalaryEmployeeDto> serviceResponse = new ServiceResponse<SalaryEmployeeDto>();
            try
            {
                IEnumerable<Salary> salaries = await _salaryRepository.GetAll();
                IList<SalaryDto> salariesMapper = _mapper.Map<IList<SalaryDto>>(salaries.ToList());
                serviceResponse.DataList = _mapper.Map<IList<SalaryEmployeeDto>>(salariesMapper);
                serviceResponse.Success = true;
            }
            catch (Exception ex)
            {
                serviceResponse.Message = ex.Message;
            }
            return serviceResponse;
        }
    }
}
