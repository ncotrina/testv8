﻿using AutoMapper;
using Salaries.Core.DTOs;
using Salaries.Core.Interfaces;
using Salaries.Core.Utilities;
using Salaries.Entity;
using Salaries.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Salaries.Core.Services
{
    public class PositionService : IPositionService
    {
        private readonly IPositionRepository _positionRepository;
        private readonly IMapper _mapper;

        public PositionService(IPositionRepository positionRepository, IMapper mapper)
        {
            _positionRepository = positionRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResponse<BaseEntityDto>> GetAll()
        {
            ServiceResponse<BaseEntityDto> serviceResponse = new ServiceResponse<BaseEntityDto>();
            try
            {
                IEnumerable<Position> divisions = await _positionRepository.GetAll();
                serviceResponse.DataList = _mapper.Map<IList<BaseEntityDto>>(divisions);
                serviceResponse.Success = true;
            }
            catch (Exception ex)
            {
                serviceResponse.Message = ex.Message;
            }
            return serviceResponse;
        }
    }
}
