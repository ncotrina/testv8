﻿using System.Collections.Generic;

namespace Salaries.Core.DTOs
{
    public class SalaryListDto
    {
        public IList<SalaryDto> SalaryListsDto { get; set; }
    }
}
