﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Salaries.Core.DTOs
{
    public class SalaryDto : IValidatableObject
    {
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public Int32 Year { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, 12, ErrorMessage = "El campo {0} debe estar en el rango de {1} a {2}")]
        public Int32 Month { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(maximumLength: 10, ErrorMessage = "El campo {0} debe tener como máximo {1} caracteres.")]
        public String EmployeeCode { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(maximumLength: 150, ErrorMessage = "El campo {0} debe tener como máximo {1} caracteres.")]
        public String EmployeeName { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(maximumLength: 150, ErrorMessage = "El campo {0} debe tener como máximo {1} caracteres.")]
        public String EmployeeSurname { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, 20, ErrorMessage = "El campo {0} debe estar en el rango de {1} a {2}")]
        public Int32 Grade { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public DateTime BeginDate { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public DateTime Birthday { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(maximumLength: 10, ErrorMessage = "El campo {0} debe tener como máximo {1} caracteres.")]
        public String IdentificationNumber { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public Decimal BaseSalary { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public Decimal ProductionBonus { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public Decimal CompensationBonus { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public Decimal Commission { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public Decimal Contributions { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]

        public BaseEntityDto Position { get; set; }
        public BaseEntityDto Office { get; set; }
        public BaseEntityDto Division { get; set; }

        /// <summary>
        /// Attribute Calculate
        /// </summary>
        public Decimal TotalSalary { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Year != DateTime.Now.Year) yield return new ValidationResult("El campo Year no pertenece al año actual.", new String[] { nameof(Year) });
            if (BeginDate == DateTime.MinValue) yield return new ValidationResult("El campo BeginDate no tiene una fecha correcta.", new String[] { nameof(BeginDate) });
            if (Birthday == DateTime.MinValue) yield return new ValidationResult("El campo Birthday no tiene una fecha correcta.", new String[] { nameof(Birthday) });
            if (BaseSalary <= 0) yield return new ValidationResult("El campo BaseSalary debe ser mayor a 0.", new String[] { nameof(BaseSalary) });
            if (ProductionBonus <= 0) yield return new ValidationResult("El campo ProductionBonus debe ser mayor a 0.", new String[] { nameof(ProductionBonus) });
            if (CompensationBonus <= 0) yield return new ValidationResult("El campo CompensationBonus debe ser mayor a 0.", new String[] { nameof(CompensationBonus) });
            if (Commission <= 0) yield return new ValidationResult("El campo Commission debe ser mayor a 0.", new String[] { nameof(Commission) });
            if (Contributions <= 0) yield return new ValidationResult("El campo Contributions debe ser mayor a 0.", new String[] { nameof(Contributions) });
        }
    }
}
