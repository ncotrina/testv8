﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Salaries.Core.DTOs
{
    public class BaseEntityDto : IValidatableObject
    {
        [Required(ErrorMessage = "El campo {0} es requerido")]
        public Int16 Id { get; set; }
        public String Name { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {            
            if (Id <= 0) yield return new ValidationResult("El campo Id debe ser mayor a 0.", new String[] { nameof(Id) });
        }
    }
}
