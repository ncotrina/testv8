﻿using System;

namespace Salaries.Core.DTOs
{
    public class SalaryFilterDto
    {
        public Int16 FilterType { get; set; }
        public Int16 Position { get; set; }
        public Int16 Office { get; set; }
        public Int16 Division { get; set; }
        public Int16 Grade { get; set; }
        public String EmployeeCode { get; set; }
    }
}
