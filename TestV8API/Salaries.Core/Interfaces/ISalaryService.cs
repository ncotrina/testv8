﻿using Salaries.Core.DTOs;
using Salaries.Core.Utilities;
using System.Threading.Tasks;

namespace Salaries.Core.Interfaces
{
    public interface ISalaryService
    {
        Task<ServiceResponse<SalaryEmployeeDto>> GetAll();
        Task<ServiceResponse<SalaryDto>> InsertMasive(SalaryListDto salaryListDto);
        ServiceResponse<SalaryEmployeeDto> GetByFilters(SalaryFilterDto filter);
        Task<ServiceResponse<SalaryEmployeeDto>> GetByEmployee(SalaryFilterDto filter);
    }
}
