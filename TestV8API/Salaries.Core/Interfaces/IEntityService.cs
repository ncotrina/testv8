﻿using Salaries.Core.DTOs;
using Salaries.Core.Utilities;
using System.Threading.Tasks;

namespace Salaries.Core.Interfaces
{
    public interface IEntityService
    {
        Task<ServiceResponse<BaseEntityDto>> GetAll();
    }
}
