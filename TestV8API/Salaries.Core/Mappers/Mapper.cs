﻿using AutoMapper;
using Salaries.Core.DTOs;
using Salaries.Entity;
using System.Collections.Generic;

namespace Salaries.Core.Mappers
{
    public class Mapper : Profile
    {
        public Mapper()
        {
            CreateMap<Salary, SalaryDto>().ReverseMap()
                .ForPath(s => s.Position.Name, opt => opt.Ignore())
                .ForPath(s => s.Office.Name, opt => opt.Ignore())
                .ForPath(s => s.Division.Name, opt => opt.Ignore()); 
            CreateMap<Office, BaseEntityDto>().ReverseMap();
            CreateMap<Division, BaseEntityDto>().ReverseMap();
            CreateMap<Position, BaseEntityDto>().ReverseMap();
            CreateMap<SalaryListDto, List<Salary>>().ReverseMap();
            CreateMap<SalaryDto, SalaryEmployeeDto>()
                 .ForMember(d => d.EmployeeFullName, opt => opt.MapFrom(src => $"{src.EmployeeName} {src.EmployeeSurname}"))
                 .ReverseMap();

        }
    }
}
